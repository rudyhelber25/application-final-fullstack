import {Routes} from '@angular/router';
import {PanelNotFoundComponent} from './panel/panel-not-found/panel-not-found.component';

export const APP_ROUTES_CONFIG: Routes = [
  {
    path: 'contact',
    loadChildren: './contact/contact.module#ContactModule'
  },
  {
    path: 'company',
    loadChildren: './company/company.module#CompanyModule',
  },
  {
    path: 'user',
    loadChildren: './user/user.module#UserModule',
  },
  {
    path: '',
    redirectTo: '/user',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PanelNotFoundComponent
  }
];
