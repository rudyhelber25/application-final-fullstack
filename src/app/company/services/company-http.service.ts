import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../bootstrap/http.service';
import {Observable} from 'rxjs';
import {CompanyRequest} from './body-request/company.request';

@Injectable()
export class CompanyHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/users/public/companies';
  }

  public doPost(request: CompanyRequest): Observable<object> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
