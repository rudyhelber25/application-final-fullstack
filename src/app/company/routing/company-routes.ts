import {Routes} from '@angular/router';
import {CompanyComponent} from '../company-form/company.component';

export const COMPANY_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: CompanyComponent,
    children: [
    ]
  }
];
