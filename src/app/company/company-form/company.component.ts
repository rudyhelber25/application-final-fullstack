import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {emailValidator} from '../../user/user-login/validators/email.validator';
import {Router} from '@angular/router';
import {CompanyHttpService} from '../services/company-http.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class CompanyComponent implements OnDestroy {

  public companyForm: FormGroup;

  public readonly validDomains = ['gmail', 'hotmail', 'reqres'];
  public readonly passMinLength = 6;
  public readonly nameMaxLength = 100;

  public thereAreErrors: boolean;

  private timeout;
  private companySubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private companyHttpService: CompanyHttpService,
              private route: Router) {
    this.thereAreErrors = false;
    this.timeout = null;

    this.companyForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(this.nameMaxLength)
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          emailValidator(this.validDomains)
        ]
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(this.passMinLength)
        ]
      ],
      conpassword: [
        '',
        [
          Validators.required,
          Validators.minLength(this.passMinLength)
        ]
      ]
    });
  }

  public onSubmit(): void {
    if (this.companyForm.valid) {
      this.companySubscription = this.companyHttpService.doPost(this.companyForm.value).subscribe(
        (response: { token: string }) => {
          // this.authService.setToken(response.token);
          this.route.navigate(['user']);

          // console.log(this.authService.getToken(), response.token);
        }
      );
    } else {
      console.log('there is some errors in the form');
    }
  }

  ngOnDestroy(): void {
    this.clearTimeout();
    this._unsubscribe(this.companySubscription);
  }

  private clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
