import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {USER_ROUTES_CONFIG} from './user-routes';


const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(USER_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
