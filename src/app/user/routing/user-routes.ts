import {Routes} from '@angular/router';
import {UserLoginComponent} from '../user-login/user-login.component';

export const USER_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: UserLoginComponent,
    children: [
    ]
  }
];
