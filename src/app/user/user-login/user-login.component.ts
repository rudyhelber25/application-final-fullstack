import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {emailValidator} from './validators/email.validator';
import {LoginHttpService} from '../services/login-http.service';
import {Subscription} from 'rxjs';
import {AuthenticationService} from '../services/authentication.service';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.ShadowDom
})
export class UserLoginComponent implements OnDestroy {
  public loginForm: FormGroup;

  public readonly validDomains = ['gmail', 'hotmail', 'reqres'];
  public readonly passMinLength = 6;

  public thereAreErrors: boolean;

  private timeout;
  private loginSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private loginHttpService: LoginHttpService,
              private authHttpService: AuthenticationService,
              private route: Router,
              private cdRef: ChangeDetectorRef) {

    this.thereAreErrors = false;
    this.timeout = null;

    this.loginForm = this.formBuilder.group({
      username: [
        'angular2000@hotmail.com',
        [
          Validators.required,
          emailValidator(this.validDomains)
        ]
      ],
      password: ['angular2000',
        [
          Validators.required,
          Validators.minLength(this.passMinLength)
        ]
      ]
    });
  }
  public onSubmit(): void {
    if (this.loginForm.valid) {
     this.loginSubscription = this.loginHttpService.doPost(this.loginForm.value).subscribe(
        (response: { token: string }) => {
          this.route.navigate(['contact']);
          this.authHttpService.setToken(response.token);
          this.cdRef.markForCheck();
        }
      );
    } else {
      console.log('there is some errors in the form');
    }
  }

  ngOnDestroy(): void {
    this.clearTimeout();
    this._unsubscribe(this.loginSubscription);
  }

  private clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }
}
