import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PanelNotFoundComponent } from './panel-not-found.component';

describe('PanelNotFoundComponent', () => {
  let component: PanelNotFoundComponent;
  let fixture: ComponentFixture<PanelNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PanelNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PanelNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
