import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panel-not-found',
  templateUrl: './panel-not-found.component.html',
  styleUrls: ['./panel-not-found.component.css']
})
export class PanelNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
