import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-panel-navigation',
  templateUrl: './panel-navigation.component.html',
  styleUrls: ['./panel-navigation.component.css']
})
export class PanelNavigationComponent implements OnInit {

  public navigationItems: PanelMenuItem[];

  constructor() {
    this.navigationItems = [];
  }

  ngOnInit() {
    this.navigationItems = [
      {
        label: 'Contact',
        route: '/contact'
      },
      {
        label: 'Company',
        route: '/company'
      },
      {
        label: 'Login',
        route: '/user'
      }
    ];
  }

}
