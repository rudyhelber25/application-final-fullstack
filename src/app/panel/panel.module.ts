import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PanelMainComponent} from './panel-main/panel-main.component';
import {PanelHeaderComponent} from './panel-header/panel-header.component';
import {PanelBodyComponent} from './panel-body/panel-body.component';
import {PanelNavigationComponent} from './panel-navigation/panel-navigation.component';
import {AppRoutingModule} from '../app-routing.module';
import {PanelNotFoundComponent} from './panel-not-found/panel-not-found.component';

@NgModule({
  declarations: [
    PanelMainComponent,
    PanelHeaderComponent,
    PanelBodyComponent,
    PanelNavigationComponent,
    PanelNotFoundComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ],
  exports: [
    PanelMainComponent
  ]
})
export class PanelModule { }
