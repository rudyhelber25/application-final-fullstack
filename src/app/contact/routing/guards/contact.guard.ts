import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router, Route, UrlSegment, CanLoad} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticationService} from '../../../user/services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ContactGuard implements CanLoad {

  constructor(private router: Router,
              private authService: AuthenticationService) {
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    const token = this.authService.getToken();
    if (token) {
      return true;
    }
    this.router.navigate(['user']);
    return false;

  }
}
