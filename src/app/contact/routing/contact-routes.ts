import {Routes} from '@angular/router';
import {ContactComponent} from '../contact-form/contact.component';
import {ContactGuard} from './guards/contact.guard';

export const CONTACT_ROUTES_CONFIG: Routes = [
  {
    path: '',
    component: ContactComponent,
    children: [
    ]
  }
];
