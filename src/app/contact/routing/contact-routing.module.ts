import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CONTACT_ROUTES_CONFIG} from './contact-routes';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forChild(CONTACT_ROUTES_CONFIG)],
  exports: [RouterModule]
})
export class ContactRoutingModule { }
