export interface ContactRequest {
  name: string;
  email: string;
  information: string;
}
