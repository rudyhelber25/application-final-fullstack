import {Injectable, Injector} from '@angular/core';
import {HttpService} from '../../bootstrap/http.service';
import {Observable} from 'rxjs';
import {ContactRequest} from './body-request/contact.request';

@Injectable()
export class ContactHttpService extends HttpService {

  constructor(private _injector: Injector) {
    super();
  }

  public path(): string {
    return '/api/contact/secure/contacts';
  }

  public doPost(request: ContactRequest): Observable<object> {
    return this.httpClient().post(this.getUrl(), request);
  }

  protected injector(): Injector {
    return this._injector;
  }
}
