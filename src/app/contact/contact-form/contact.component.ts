import {Component, OnDestroy, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {emailValidator} from '../../user/user-login/validators/email.validator';
import {Subscription} from 'rxjs';
import {ContactHttpService} from '../services/contact-http.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class ContactComponent implements OnDestroy {

  public contactForm: FormGroup;

  public readonly validDomains = ['gmail', 'hotmail', 'reqres'];
  public readonly nameMaxLength = 100;
  public readonly infMaxLength = 255;

  public thereAreErrors: boolean;

  private timeout;
  private contactSubscription: Subscription;

  constructor(private formBuilder: FormBuilder,
              private contactHttpService: ContactHttpService,
              private route: Router) {
    this.thereAreErrors = false;
    this.timeout = null;

    this.contactForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(this.nameMaxLength)
        ]
      ],
      email: [
        '',
        [
          Validators.required,
          emailValidator(this.validDomains)
        ]
      ],
      information: [
        '',
        [
          Validators.required,
          Validators.maxLength(this.infMaxLength)
        ]
      ]
    });
  }

  public onSubmit(): void {
    if (this.contactForm.valid) {
      this.contactSubscription = this.contactHttpService.doPost(this.contactForm.value).subscribe(
        (response: { token: string }) => {
          // this.authService.setToken(response.token);
          this.route.navigate(['contact']);

          // console.log(this.authService.getToken(), response.token);
        }
      );
    } else {
      console.log('there is some errors in the form');
    }
  }

  ngOnDestroy(): void {
    this.clearTimeout();
    this._unsubscribe(this.contactSubscription);
  }

  private clearTimeout(): void {
    if (this.timeout) {
      clearTimeout(this.timeout);
      this.timeout = null;
    }
  }

  private _unsubscribe(subscription: Subscription): void {
    if (subscription) {
      subscription.unsubscribe();
      subscription = null;
    }
  }

}
